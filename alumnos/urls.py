from django.urls import path
from . import views

urlpatterns = [
    path('alumnos/list', views.alumnos_list, name='alumnos_list'),
    path('alumnos/<int:pk>/', views.alumnos_detail, name='alumnos_detail'),
    path('alumnos/<int:pk>/edit/', views.alumnos_edit, name='alumnos_edit'),
    path('alumnos/new', views.alumnos_new, name='alumnos_new'),
    
    path('alumnos/<pk>/aprobar/', views.alumnos_aprobar, name='alumnos_aprobar'),
    path('alumnos/<pk>/remove/', views.alumnos_remove, name='alumnos_remove'),
    path('alumnos/<str:curso>/curso', views.alumnos_curso_list, name='alumnos_curso_list'),
]
