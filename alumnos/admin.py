from django.contrib import admin

from alumnos.models import Alumnos

# Register your models here.
class AlumnosAdmin(admin.ModelAdmin):
    

    list_filter = ['apellidos','aprobado'] # 
    list_display_links = ['pk']
    list_display =(
        'pk',
        'apellidos',
        'nombres',
        'curso',
    )
    list_editable = ( 
        #'apellidos',
        'nombres',
    )


admin.site.register(Alumnos, AlumnosAdmin)
