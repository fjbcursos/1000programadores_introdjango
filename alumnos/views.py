from django.shortcuts import render
from django.views.generic import ListView, FormView
from alumnos.models import Alumnos
from alumnos.forms import ABMAlumnoForm

from django.utils import timezone
from django.shortcuts import render, get_object_or_404, redirect
from django.views.decorators.csrf import csrf_exempt

# NOTE Vistas de Funciones
def alumnos_list(request):
    alumnos = Alumnos.objects.all()
    return render(request, 'alumnos/alumnos_list.html', {'Alumnos': alumnos})

def alumnos_curso_list(request, curso):
    alumnos = Alumnos.objects.filter(curso=curso).order_by('apellidos')
    return render(request, 'alumnos/alumnos_list.html', {'Alumnos': alumnos})

def alumnos_detail(request, pk):
    alumnos = get_object_or_404(Alumnos, pk=pk)
    #alumnos = Alumnos.objects.get(pk=pk)
    return render(request, 'alumnos/alumnos_detail.html', {'Alumno': alumnos})

@csrf_exempt
def alumnos_new(request):
    if request.method == "POST":
        form = ABMAlumnoForm(request.POST)
        if form.is_valid():
            alumnos = form.save(commit=False)
            alumnos.save()
            return redirect('alumnos_detail', pk=alumnos.pk)
    else:
        form = ABMAlumnoForm()
    return render(request, 'alumnos/alumnos_edit.html', {'form': form})
@csrf_exempt
def alumnos_edit(request, pk):
    alumnos = get_object_or_404(Alumnos, pk=pk)
    if request.method == "POST":
        form = ABMAlumnoForm(request.POST, instance=alumnos)
        if form.is_valid():
            post = form.save(commit=False)
            #post.author = request.user
            # post.published_date = timezone.now()
            post.save()
            return redirect('alumnos_detail', pk=alumnos.pk)
    else:
        form = ABMAlumnoForm(instance=alumnos)
    return render(request, 'alumnos/alumnos_edit.html', {'form': form})

def alumnos_aprobar(request, pk):
    alumno = get_object_or_404(Alumnos, pk=pk)
    alumno.aprobar()
    return redirect('alumnos_detail', pk=pk)

def alumnos_remove(request, pk):
    alumnos = get_object_or_404(Alumnos, pk=pk)
    alumnos.delete()
    return redirect('alumnos_list')

# NOTE Vistas de clases

class AlumnosListView(ListView):
    model = Alumnos
