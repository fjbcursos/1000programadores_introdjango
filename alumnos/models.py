from django.db import models

# Create your models here.
class Alumnos(models.Model):

    apellidos = models.CharField( max_length=50)
    nombres = models.CharField( max_length=50)
    #curso = models.CharField(max_length=1,db_column="curso_id")
    curso =  models.ForeignKey("cursos.Curso",db_column="curso_id", verbose_name="Cursos", on_delete=models.CASCADE)
    dni = models.IntegerField(default=0)
    fecha_nacimiento = models.DateField(auto_now=False, auto_now_add=False)
    observaciones = models.TextField(blank=True, null=True)
    aprobado =  models.BooleanField(default=False)
    class Meta:
        verbose_name = "Alumnos"
        verbose_name_plural = "Alumnos"

    def aprobar(self):
        self.aprobado = True
        self.save()
        
    def __str__(self):
        return f"{self.apellidos}, {self.nombres}"

