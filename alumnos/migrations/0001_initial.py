# Generated by Django 4.0.4 on 2022-04-29 21:17

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Alumnos',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('apellidos', models.CharField(max_length=50)),
                ('nombres', models.CharField(max_length=50)),
                ('curso', models.CharField(db_column='curso_id', max_length=1)),
                ('dni', models.IntegerField(default=0)),
                ('fecha_nacimiento', models.DateField()),
                ('observaciones', models.TextField(blank=True, null=True)),
            ],
            options={
                'verbose_name': 'Alumnos',
                'verbose_name_plural': 'Alumnos',
            },
        ),
    ]
