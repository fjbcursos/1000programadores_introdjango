from django.urls import path
from . import views

urlpatterns = [
    path('cursos/list', views.cursos_list.as_view(), name='cursos_list'),
]
