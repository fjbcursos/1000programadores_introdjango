from django.contrib import admin

# Register your models here.
from django.contrib import admin

from cursos.models import Curso

# Register your models here.
class CursoAdmin(admin.ModelAdmin):
    pass

    # list_filter = ['Materia', 'Condicion','Fecha']
    # list_display =(
    #     'pk',
    #     'Materia',
    #     'Condicion',
    #     'Fecha',
    # )


admin.site.register(Curso, CursoAdmin)
