from mailbox import mboxMessage
from django.db import models

# Create your models here.

class Curso(models.Model):

    nombre = models.CharField( max_length=50, unique=True)
    habilitado = models.BooleanField(default=True)

    class Meta:
        verbose_name = "Curso"
        verbose_name_plural = "Cursos"
        db_table = "cursos"
        managed=True

    def __str__(self):
        return f"({self.pk}) {self.nombre}"
